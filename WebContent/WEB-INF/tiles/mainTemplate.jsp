<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ORIX 高圧</title>
	<link type="text/css" rel="stylesheet" href="/resource/css/common.css">
	
	<script type="text/javascript" src="/resource/js/common.js"></script>
	<script type="text/javascript" src="/resource/js/jquery-3.1.1.min.js"></script>
</head>
<body>
	<tiles:insertAttribute name="header"/>
	<tiles:insertAttribute name="body"/>
	<tiles:insertAttribute name="bottom"/>
</body>
</html>